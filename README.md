# postgres_help_stuff
Just some snippets for me to run

# prepare and checkout
    mkdir -p $HOME/workspace
    git clone git@github.com:mziescha/postgres.git
    cd postgres

# configure

    ./configure --prefix=$(pwd) --enable-depend --enable-cassert --enable-debug

    optional

    ./configure --prefix=$(pwd) --enable-depend --enable-cassert --enable-debug \
        --with-blocksize=BLOCKSIZE --with-segsize=SEGSIZE --with-wal-blocksize=BLOCKSIZ

# PATH and PGDATA

    export PATH=$(pwd)/bin:$PATH
    export PGDATA=$(pwd)/db_data

# new db data dir

    initdb
    
# start db daemon

    pg_ctl -D $(pwd)/db_data -l logfile start

# start in foreground

    postgres -D /home/mziescha/Workspace/postgres/db_data
